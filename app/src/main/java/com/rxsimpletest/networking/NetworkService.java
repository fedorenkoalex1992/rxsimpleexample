package com.rxsimpletest.networking;

import com.rxsimpletest.App;
import com.rxsimpletest.models.WeatherResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;


/**
 * Created by fedor on 04.01.2017.
 */

public interface NetworkService {

    @GET("weather?id=" + App.CITY_CODE + "&appid=" + App.API_KEY + "&units=metric")
    Observable<WeatherResponse> getWeather();
}
