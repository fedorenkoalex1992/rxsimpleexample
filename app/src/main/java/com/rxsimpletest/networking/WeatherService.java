package com.rxsimpletest.networking;

import com.rxsimpletest.models.WeatherResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by fedor on 04.01.2017.
 */

public class WeatherService {

    private final NetworkService mNetworkService;

    public WeatherService(NetworkService networkService) {
        this.mNetworkService = networkService;
    }


    public Observable<WeatherResponse> getWeather() {
        return mNetworkService.getWeather()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    return Observable.error(throwable);
                });

    }
}
