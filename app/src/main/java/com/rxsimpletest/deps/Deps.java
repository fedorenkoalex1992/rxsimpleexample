package com.rxsimpletest.deps;

import com.rxsimpletest.home.HomeActivity;
import com.rxsimpletest.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fedor on 04.01.2017.
 */


@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(HomeActivity homeActivity);
}
