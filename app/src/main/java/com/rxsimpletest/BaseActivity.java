package com.rxsimpletest;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.rxsimpletest.deps.DaggerDeps;
import com.rxsimpletest.deps.Deps;

import com.rxsimpletest.networking.NetworkModule;

/**
 * Created by fedor on 04.01.2017.
 */

public class BaseActivity extends AppCompatActivity {


    private  Deps mDependencies;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDependencies = DaggerDeps.builder().networkModule(new NetworkModule()).build();
    }


    public Deps getDependencies() {
        return mDependencies;
    }
}
