package com.rxsimpletest.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;


public class Dialoger {

    private Context context;
    private static Dialoger instance;
    private AlertDialog.Builder builder;

    public Dialoger(Context c) {
        context = c;
        builder = new AlertDialog.Builder(context);
    }

    public static Dialoger getInstance(Context c) {
        // if (instance == null)
        // {
        instance = new Dialoger(c);
        // }
        return instance;
    }

    public Dialoger withTitle(String s) {
        builder.setTitle(s);
        return instance;
    }

    public Dialoger withTitle(int resId) {
        builder.setTitle(resId);
        return instance;
    }

    public Dialoger withMessage(String s) {
        builder.setMessage(s);
        return instance;
    }

    public Dialoger withMessage(int resId) {
        builder.setMessage(resId);
        return instance;
    }

    public Dialoger cacnelable(boolean cancelable) {
        builder.setCancelable(cancelable);
        return instance;
    }
//
//    public Dialoger withCancelListener(DialogInterface.OnDismissListener listener) {
//        builder.setOnDismissListener(listener);
//        return instance;
//    }

    public Dialoger withMessage(String format, Object... args) {
        builder.setMessage(String.format(format, args));
        return instance;
    }

    public Dialoger addCancelButton(String text) {
        builder.setNegativeButton(text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return instance;
    }

    public Dialoger addCancelButton(int text) {
        builder.setNegativeButton(text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return instance;
    }

    public Dialoger addButton(String text, DialogInterface.OnClickListener listener) {
        builder.setNeutralButton(text, listener);
        return instance;
    }

    public Dialoger addButton(int text, DialogInterface.OnClickListener listener) {
        builder.setNeutralButton(text, listener);
        return instance;
    }

    public Dialoger addButtonPositive(int text, DialogInterface.OnClickListener listener) {
        builder.setPositiveButton(text, listener);
        return instance;
    }

    public void show() {
        try {

            builder.show();
        } catch (Exception e) {
        }

    }

}
