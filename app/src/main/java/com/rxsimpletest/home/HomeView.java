package com.rxsimpletest.home;

import com.rxsimpletest.models.WeatherResponse;

/**
 * Created by fedor on 04.01.2017.
 */

public interface HomeView {

    void showLoading();

    void removeLoading();

    void onFailure(String errorMessage);

    void getWeatherSuccess(WeatherResponse response);
}
