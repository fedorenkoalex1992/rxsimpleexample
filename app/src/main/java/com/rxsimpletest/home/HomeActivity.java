package com.rxsimpletest.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rxsimpletest.BaseActivity;
import com.rxsimpletest.R;
import com.rxsimpletest.models.WeatherResponse;
import com.rxsimpletest.networking.WeatherService;
import com.rxsimpletest.utils.Dialoger;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity implements HomeView {


    @Inject
    public WeatherService mWeatherService;


    private ProgressBar mProgressBar;
    private TextView mCityName;
    private TextView mTemperature;
    private TextView mPressure;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDependencies().inject(this);

        renderView();

        HomePresenter homePresenter = new HomePresenter(mWeatherService, this);
        homePresenter.getWeatherData();

    }

    public void renderView() {
        setContentView(R.layout.activity_main);
        mProgressBar = (ProgressBar) findViewById(R.id.homeProgressBar);
        mCityName = (TextView) findViewById(R.id.cityName);
        mTemperature = (TextView) findViewById(R.id.tempNow);
        mPressure = (TextView) findViewById(R.id.pressureNow);
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String errorMessage) {
        Dialoger.getInstance(this).withMessage("Error: " + errorMessage).addCancelButton("OK").show();
    }

    @Override
    public void getWeatherSuccess(WeatherResponse response) {
        Log.d("weather_result", "Success " + response.getName());
        mCityName.setText(response.getName());
        mTemperature.setText("Temperature: " + response.getMain().getTemp());
        mPressure.setText("Pressure: " + response.getMain().getPressure());
    }
}
