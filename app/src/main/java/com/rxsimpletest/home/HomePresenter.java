package com.rxsimpletest.home;

import android.util.Log;

import com.rxsimpletest.models.WeatherResponse;
import com.rxsimpletest.networking.WeatherService;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by fedor on 04.01.2017.
 */

public class HomePresenter {

    private final WeatherService mWeatherService;
    private final HomeView mHomeView;
    private CompositeDisposable mSubscription;

    public HomePresenter(WeatherService service, HomeView view) {
        this.mWeatherService = service;
        this.mHomeView = view;
        this.mSubscription = new CompositeDisposable();

    }

    public void getWeatherData() {
        mHomeView.showLoading();


        Disposable subscription = mWeatherService.getWeather().subscribe(
                response -> {
                    mHomeView.removeLoading();
                    mHomeView.getWeatherSuccess(response);
                }, throwable -> {
                    mHomeView.removeLoading();
                    mHomeView.onFailure(throwable.getMessage());
                });
        mSubscription.add(subscription);
    }

    public void onStop() {
        mSubscription.dispose();
    }
}
